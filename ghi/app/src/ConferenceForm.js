import React, {useEffect, useState} from 'react';

function ConferenceForm() {
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maximumPresentations, setMaximumPresentations] = useState('');
    const [maximumAttendees, setMaximumAttendees] = useState('');
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState('')
  const fetchData = async () => {

    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations)   
    }
  }
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleStartsChange = (event) => {
    const value = event.target.value;
    setStarts(value);
  }

  const handleEndsChange = (event) => {
    const value = event.target.value;
    setEnds(value);
  }

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }

  const handleMaximumPresentationsChange = (event) => {
    const value = event.target.value;
    setMaximumPresentations(value);
  }

  const handleMaximumAttendeesChange = (event) => {
    const value = event.target.value;
    setMaximumAttendees(value);
  }
  
  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

    const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = maximumPresentations;
    data.max_attendees = maximumAttendees;
    data.location = location;


    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        },
    };

    const response = await fetch(conferenceUrl, fetchConfig);

    if (response.ok) {
        const newConference = await response.json();
        console.log(newConference)
    
        setName('');
        setStarts('');
        setEnds('');
        setDescription('');
        setMaximumPresentations('');
        setMaximumAttendees('');
        setLocation('');
    }
    }
console.log("seeing this")

  useEffect(() => {
    fetchData();
  }, []);

  return(
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new conference</h1>
        <form onSubmit={handleSubmit} id="create-conference-form">
          <div className="form-floating mb-3">
            <input placeholder="Name" onChange={handleNameChange} value={name} required type="text" name="name" id="name" className="form-control"/>
            <label htmlFor="name">Name</label>
          </div>
          <div className="form-floating mb-3">
            <input placeholder="Starts" onChange={handleStartsChange} value={starts} required type="date" name="starts" id="starts" className="form-control"/>
            <label htmlFor="starts">Starts</label>
          </div>
          <div className="form-floating mb-3">
            <input placeholder="Ends" onChange={handleEndsChange} value={ends} required type="date" name="ends" id="ends" className="form-control"/>
            <label htmlFor="ends">Ends</label>
          </div>
          <div className="form-floating mb-3">
            <input placeholder="Description" onChange={handleDescriptionChange} value={description} required type="text" name="description" id="description" className="form-control"/>
            <label htmlFor="description">Description</label>
          </div>
          <div className="form-floating mb-3">
            <input placeholder="maximumPresentations" onChange={handleMaximumPresentationsChange} value={maximumPresentations} required type="number" name="maximumPresentations" id="maximumPresentations" className="form-control"/>
            <label htmlFor="maximumPresentations">Maximum Presentations</label>
          </div>
          <div className="form-floating mb-3">
            <input placeholder="maximumAttendees" onChange={handleMaximumAttendeesChange} value={maximumAttendees} required type="number" name="maximumAttendees" id="maximumAttendees" className="form-control"/>
            <label htmlFor="maximumAttendees">Maximum Attendees</label>
          </div>
          <div className="mb-3">
     
   
              <select required name="location" onChange={handleLocationChange} value={location} id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                    return (
                    <option key={location.id} value={location.id}>
                        {location.name}
                    </option>
                    );
                })}
            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
);
}

export default ConferenceForm;
