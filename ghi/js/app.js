function createCard(name, description, pictureUrl, start, end, location) {
    return `

 
    <div class="card mb-2">
        <div class="card shadow mt-20.5">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="text-secondary">${location}</p>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
        ${start} - ${end}
        </div>
        </div>

   
      </div>  
      </div>
      </div>
      </div>
    `;
  }


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
  
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const dateStart = new Date(details.conference.starts);
            const dateEnd = new Date(details.conference.ends);
            const start = (dateStart.getMonth() + 1) + "/" + (dateStart.getDate() + 1) + "/" + dateStart.getFullYear();
            const end = (dateEnd.getMonth() + 1) + "/" + (dateEnd.getDate() + 1) + "/" + dateEnd.getFullYear();
            const location = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, start, end, location);
            const column = document.querySelector('.col');
            column.innerHTML += html;
            const row = document.querySelector('.row');
            row.appendChild(column)
          

            
          }
        }
  
      }
    } catch (e) {
      // Figure out what to do if an error is raised
    }
  
  });
